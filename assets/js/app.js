/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 3);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.initTab = initTab;
exports.changeTab = changeTab;
var tabs = void 0;
var tabClassContent = "tab__content";
var tabSelectedClassItem = "tab__item--selected";
var tabSelectedClassContent = "tab__content--selected";

function initTab() {
    tabs = document.querySelectorAll(".tab__item");
    for (var i = 0; i < tabs.length; i++) {
        registerTabListener(tabs[i], "click");
    }
}

function registerTabListener(tab, event) {
    tab.addEventListener(event, function () {
        var dataTarget = tab.getAttribute("data-target");
        var target = document.querySelector("#" + dataTarget);

        closeTabAll();
        setTabUnselectedAll();

        showTabTarget(target);
        setTabSelected(tab);
    });
}

function showTabTarget(target) {
    if (target != null && target != undefined) target.classList.add(tabSelectedClassContent);
}

function closeTabTarget(target) {
    if (target != null && target != undefined) target.classList.remove(tabSelectedClassContent);
}

function closeTabTargetAll() {
    for (var i = 0; i < tabs.length; i++) {
        var dataTarget = tabs[i].getAttribute("data-target");
        var target = document.querySelector("#" + dataTarget);
        closeTabTarget(target);
    }
}

function closeTabAll() {
    var tabContents = document.querySelectorAll("." + tabClassContent);
    for (var i = 0; i < tabContents.length; i++) {
        closeTabTarget(tabContents[i]);
    }
}

function setTabSelected(tab) {
    tab.classList.add(tabSelectedClassItem);
}

function setTabUnselected(tab) {
    tab.classList.remove(tabSelectedClassItem);
}

function setTabUnselectedAll() {
    for (var i = 0; i < tabs.length; i++) {
        setTabUnselected(tabs[i]);
    }
}

function changeTab(dataTarget) {
    var target = document.querySelector("#" + dataTarget);
    closeTabAll();
    showTabTarget(target);
}

/***/ }),
/* 1 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.updateUI = updateUI;
exports.updateProfile = updateProfile;
exports.switchProfileEdit = switchProfileEdit;
exports.switchProfileView = switchProfileView;

var _tab = __webpack_require__(0);

var tab = _interopRequireWildcard(_tab);

var _template = __webpack_require__(5);

var template = _interopRequireWildcard(_template);

var _variable = __webpack_require__(2);

var variable = _interopRequireWildcard(_variable);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

var rootHTML = document.querySelector(".root");
var rootDesktopClass = "desktop";
var rootMobileClass = "mobile";

var contentView = "content__view";
var contentEdit = "content__edit";
var contentHide = "content--hide";

/*export function updateUI(value, cls, alt = false) {
    if (value == '') {
        if (alt) alert("Input can't be empty")
        return
    }

    if (cls == "name") {
        let fullname = value.split(" ")
        let fname = fullname[0]
        let lname = fullname.slice(1, fullname.length).join(" ")
        updateUI(fname, "fname")
        updateUI(lname, "lname")
    }

    let els = document.querySelectorAll(`.${cls}`)
    for (let i = 0; i < els.length; i++) {
        if (els[i].tagName == 'INPUT') els[i].value = value
        else els[i].innerHTML = value
    }
}*/

function updateUI() {
    if (isDesktop()) setDesktopLayout();else setMobileLayout();
    tab.initTab();
}

function updateProfile() {
    var fName = document.querySelector('#txt-first-name').value;
    var lName = document.querySelector('#txt-last-name').value;
    var website = document.querySelector('#txt-website').value;
    var phone = document.querySelector('#txt-phone').value;
    var address = document.querySelector('#txt-address').value;

    if (fName == '' && lName == '' && website == '' && phone == '' && address == '') {
        alert("Input needed for updating profile");
        return;
    }

    if (fName != '') variable.setFirstName(fName);
    if (lName != '') variable.setLastName(lName);
    if (website != '') variable.setWebsite(website);
    if (phone != '') variable.setPhone(phone);
    if (address != '') variable.setAddress(address);

    updateUI();
}

function isDesktop() {
    if (screen.width > 767) return true;
    return false;
}

function isMobile() {
    if (screen.width <= 767) return true;
    return false;
}

function setDesktopLayout() {
    var tpl = processTpl(template.getDesktopTpl());
    rootHTML.classList.remove(rootMobileClass);
    rootHTML.classList.add(rootDesktopClass);
    rootHTML.innerHTML = tpl;
}

function setMobileLayout() {
    var tpl = processTpl(template.getMobileTpl());
    rootHTML.classList.remove(rootDesktopClass);
    rootHTML.classList.add(rootMobileClass);
    rootHTML.innerHTML = tpl;
    console.log(tpl);
}

function processTpl(tpl) {
    var profileImage = variable.getProfileImage();
    var firstName = variable.getFirstName();
    var lastName = variable.getLastName();
    var website = variable.getWebsite();
    var phone = variable.getPhone();
    var address = variable.getAddress();

    tpl = tpl.replace(/{profileImage}/gi, profileImage);
    tpl = tpl.replace(/{firstName}/gi, firstName);
    tpl = tpl.replace(/{lastName}/gi, lastName);
    tpl = tpl.replace(/{website}/gi, website);
    tpl = tpl.replace(/{phone}/gi, phone);
    tpl = tpl.replace(/{address}/gi, address);

    return tpl;
}

function switchProfileEdit() {
    var contentViews = document.querySelectorAll("." + contentView);
    var contentEdits = document.querySelectorAll("." + contentEdit);
    var editTextInputs = document.querySelectorAll('.edit-text__text');

    for (var i = 0; i < contentViews.length; i++) {
        contentViews[i].classList.add(contentHide);
    }

    for (var _i = 0; _i < contentEdits.length; _i++) {
        contentEdits[_i].classList.remove(contentHide);
    }

    for (var _i2 = 0; _i2 < editTextInputs.length; _i2++) {
        editTextInputs[_i2].disabled = false;
    }
}

function switchProfileView() {
    var contentViews = document.querySelectorAll("." + contentView);
    var contentEdits = document.querySelectorAll("." + contentEdit);
    var editTextInputs = document.querySelectorAll('.edit-text__text');

    for (var i = 0; i < contentViews.length; i++) {
        contentViews[i].classList.remove(contentHide);
    }

    for (var _i3 = 0; _i3 < contentEdits.length; _i3++) {
        contentEdits[_i3].classList.add(contentHide);
    }

    for (var _i4 = 0; _i4 < editTextInputs.length; _i4++) {
        editTextInputs[_i4].disabled = true;
    }
}

/***/ }),
/* 2 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.setProfileImage = setProfileImage;
exports.getProfileImage = getProfileImage;
exports.setFullName = setFullName;
exports.setFirstName = setFirstName;
exports.getFirstName = getFirstName;
exports.setLastName = setLastName;
exports.getLastName = getLastName;
exports.setWebsite = setWebsite;
exports.getWebsite = getWebsite;
exports.setPhone = setPhone;
exports.getPhone = getPhone;
exports.setAddress = setAddress;
exports.getAddress = getAddress;
var profileImage = "/assets/image/profile_image.jpg";
var firstName = "Jessica";
var lastName = "Parker";
var website = "www.seller.com";
var phone = "(949) 325 - 68594";
var address = "Newport Beach, CA";

function setProfileImage(value) {
    profileImage = value;
}

function getProfileImage() {
    return profileImage;
}

function setFullName(value) {
    var fullname = value.split(" ");
    firstName = fullname[0];
    lastName = fullname.slice(1, fullname.length).join(" ");
}

function setFirstName(value) {
    firstName = value;
}

function getFirstName() {
    return firstName;
}

function setLastName(value) {
    lastName = value;
}

function getLastName() {
    return lastName;
}

function setWebsite(value) {
    website = value;
}

function getWebsite() {
    return website;
}

function setPhone(value) {
    phone = value;
}

function getPhone() {
    return phone;
}

function setAddress(value) {
    address = value;
}

function getAddress() {
    return address;
}

/***/ }),
/* 3 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _tab = __webpack_require__(0);

var tab = _interopRequireWildcard(_tab);

var _popup = __webpack_require__(4);

var popup = _interopRequireWildcard(_popup);

var _util = __webpack_require__(1);

var util = _interopRequireWildcard(_util);

var _resize = __webpack_require__(8);

var resize = _interopRequireWildcard(_resize);

var _variable = __webpack_require__(2);

var variable = _interopRequireWildcard(_variable);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

window.tab = tab;
window.popup = popup;
window.util = util;
window.variable = variable;

resize.initResize();

/***/ }),
/* 4 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.openPopup = openPopup;
exports.callPopupFunc = callPopupFunc;
exports.closePopup = closePopup;
exports.getPopupInput = getPopupInput;
var popup = document.querySelector(".popup");

var popupTitle = "popup__title";
var popupInput = "popup__input";
var popupClassOpen = "popup--open";
var popupOffsetTop = -10;
var popupOffsetLeft = 20;

var popupFunction = void 0;

function openPopup(target) {
    var title = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : "Title";
    var func = arguments[2];

    document.querySelector("." + popupTitle).innerHTML = title;

    closePopup();
    var rect = target.getBoundingClientRect();
    popup.style.top = rect.top + popupOffsetTop + "px";
    popup.style.left = rect.left + popupOffsetLeft + "px";
    popup.classList.add(popupClassOpen);

    popupFunction = func;
}

function callPopupFunc() {
    popupFunction();
    closePopup();
}

function closePopup() {
    popup.classList.remove(popupClassOpen);
    resetPopupInput();
}

function getPopupInput() {
    return document.querySelector("." + popupInput).value;
}

function resetPopupInput() {
    document.querySelector("." + popupInput).value = '';
}

/***/ }),
/* 5 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.getDesktopTpl = getDesktopTpl;
exports.getMobileTpl = getMobileTpl;

var _desktop = __webpack_require__(6);

var _desktop2 = _interopRequireDefault(_desktop);

var _mobile = __webpack_require__(7);

var _mobile2 = _interopRequireDefault(_mobile);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function getDesktopTpl() {
    return _desktop2.default;
}

function getMobileTpl() {
    return _mobile2.default;
}

/***/ }),
/* 6 */
/***/ (function(module, exports) {

module.exports = "<div class=\"header card card--no-padding\">\n    <div class=\"header-logout\">\n        <button class=\"header-logout__button-logout button button--primary button--uppercase\">LOG OUT</button>\n    </div>\n    <div class=\"header-cover\">\n        <button class=\"header-cover__button-upload button button--default\">\n                    <i class=\"header-cover__button-upload-element header-cover__button-upload-element--icon button__icon button__icon--default ion-ios-camera\"></i>\n                    <span class=\"header-cover__button-upload-element\">Upload Cover Image</span>\n                </button>\n        <div class=\"header-profile\">\n            <div class=\"header-profile__image-container header-profile__image-container--shadow\">\n                <img src=\"{profileImage}\" class=\"header-profile__image-image\" width=\"250px\" height=\"200px\">\n            </div>\n            <div class=\"header-profile__row\">\n                <div class=\"header-profile__col\">\n                    <span class=\"name header-profile__text header-profile__text--highlight\">{firstName} {lastName}</span>\n                </div>\n                <div class=\"header-profile__col\">\n                    <div class=\"review review--align-right\">\n                        <div class=\"review__star-container\">\n                            <span class=\"review__star ion-ios-star\"></span>\n                            <span class=\"review__star ion-ios-star\"></span>\n                            <span class=\"review__star ion-ios-star\"></span>\n                            <span class=\"review__star ion-ios-star\"></span>\n                            <span class=\"review__star ion-ios-star-outline\"></span>\n                        </div>\n                        <span class=\"review__text\">6</span>\n                        <span class=\"review__label\">Reviews</span>\n                    </div>\n                </div>\n            </div>\n            <div class=\"header-profile__row\">\n                <div class=\"header-profile__col header-profile__col--center-vertical\">\n                    <i class=\"header-profile__text--icon header-profile__text--center-vertical ion-ios-location-outline\"></i>\n                    <span class=\"address header-profile__text header-profile__text--center-vertical\">{address}</span>\n                </div>\n            </div>\n            <div class=\"header-profile__row\">\n                <div class=\"header-profile__col\">\n                    <i class=\"header-profile__text--icon header-profile__text--center-vertical ion-ios-telephone-outline\"></i>\n                    <span class=\"phone header-profile__text header-profile__text--center-vertical\">{phone}</span>\n                </div>\n            </div>\n        </div>\n    </div>\n    <div class=\"header-footer\">\n        <div class=\"header-footer__tab\">\n            <ul class=\"tab\">\n                <li class=\"tab__item tab__item--selected\" data-target=\"tab-desktop-about\">about</li>\n                <li class=\"tab__item\" data-target=\"tab-desktop-setting\">settings</li>\n                <li class=\"tab__item\">option 1</li>\n                <li class=\"tab__item\">option 2</li>\n                <li class=\"tab__item\">option 3</li>\n            </ul>\n        </div>\n        <div class=\"header-footer__follower\">\n            <div class=\"follower follower--align-right\">\n                <span class=\"follower__icon ion-plus-circled\"></span>\n                <span class=\"follower__text\">15</span>\n                <span class=\"follower__label\">Followers</span>\n            </div>\n        </div>\n    </div>\n</div>\n<div class=\"content card\">\n    <div id=\"tab-desktop-about\" class=\"tab__content tab__content--selected\">\n        <div class=\"content__title\">About</div>\n        <div class=\"content__content\">\n            <div class=\"edit-label\">\n                <span class=\"name edit-label__text edit-label__text--highlight\">{firstName} {lastName}</span>\n                <!--<span class=\"edit-label__button ion-edit\" onclick=\"window.popup.openPopup(this, 'Full Name', function(){ window.util.updateUI(popup.getPopupInput(), 'name', true) });\"></span>-->\n                <span class=\"edit-label__button ion-edit\" onclick=\"window.popup.openPopup(this, 'Full Name', function(){ window.variable.setFullName(popup.getPopupInput()); window.util.updateUI(); });\"></span>\n            </div>\n            <br>\n            <div class=\"edit-label\">\n                <span class=\"edit-label__icon ion-android-globe\"></span>\n                <span class=\"web edit-label__text\">{website}</span>\n                <!--<span class=\"edit-label__button ion-edit\" onclick=\"window.popup.openPopup(this, 'Website', function(){ window.util.updateUI(popup.getPopupInput(), 'web', true) });\"></span>-->\n                <span class=\"edit-label__button ion-edit\" onclick=\"window.popup.openPopup(this, 'Website', function(){ window.variable.setWebsite(popup.getPopupInput()); window.util.updateUI(); });\"></span>\n            </div>\n            <br>\n            <div class=\"edit-label\">\n                <span class=\"edit-label__icon ion-ios-telephone-outline\"></span>\n                <span class=\"phone edit-label__text\">{phone}</span>\n                <!--<span class=\"edit-label__button ion-edit\" onclick=\"window.popup.openPopup(this, 'Phone Number', function(){ window.util.updateUI(popup.getPopupInput(), 'phone', true) });\"></span>-->\n                <span class=\"edit-label__button ion-edit\" onclick=\"window.popup.openPopup(this, 'Phone Number', function(){ window.variable.setPhone(popup.getPopupInput()); window.util.updateUI(); });\"></span>\n            </div>\n            <br>\n            <div class=\"edit-label\">\n                <span class=\"edit-label__icon ion-ios-home-outline\"></span>\n                <span class=\"address edit-label__text\">{address}</span>\n                <!--<span class=\"edit-label__button ion-edit\" onclick=\"window.popup.openPopup(this, 'City, State & Zip', function(){ window.util.updateUI(popup.getPopupInput(), 'address', true) });\"></span>-->\n                <span class=\"edit-label__button ion-edit\" onclick=\"window.popup.openPopup(this, 'City, State & Zip', function(){ window.variable.setAddress(popup.getPopupInput()); window.util.updateUI(); });\"></span>\n            </div>\n            <br>\n        </div>\n    </div>\n\n    <div id=\"tab-desktop-setting\" class=\"tab__content\">\n        <div class=\"content__title\">Settings</div>\n    </div>\n</div>";

/***/ }),
/* 7 */
/***/ (function(module, exports) {

module.exports = "<div class=\"header\">\n    <div class=\"header__row header__row--align-right\">\n        <span class=\"header__logout\">log out</span>\n    </div>\n    <div class=\"header__row header__row--align-center\">\n        <div class=\"header__image-container header__image-container--shadow\">\n            <img src=\"{profileImage}\" class=\"header__image-image\" width=\"250px\" height=\"200px\">\n        </div>\n    </div>\n    <div class=\"header__row header__row--align-center\">\n        <span class=\"name header__text header__text--highlight\">{firstName} {lastName}</span>\n    </div>\n    <div class=\"header__row header__row--align-left\">\n        <span class=\"header__icon ion-ios-telephone-outline\"></span>\n        <span class=\"phone header__text\">{phone}</span>\n    </div>\n    <div class=\"header__row header__row--align-left\">\n        <span class=\"header__icon ion-ios-location-outline\"></span>\n        <span class=\"address header__text\">{address}</span>\n    </div>\n    <div class=\"header__row header__row--align-left\">\n        <div class=\"review__star-container review--primary\">\n            <span class=\"review__star ion-ios-star\"></span>\n            <span class=\"review__star ion-ios-star\"></span>\n            <span class=\"review__star ion-ios-star\"></span>\n            <span class=\"review__star ion-ios-star\"></span>\n            <span class=\"review__star ion-ios-star-outline\"></span>\n        </div>\n    </div>\n    <div class=\"header__row header__row--align-left\">\n        <span class=\"review__text review__text--no-flex\">6</span>\n        <span class=\"review__label review__label--no-flex review__label--lighten\">Reviews</span>\n    </div>\n    <div class=\"header__row header__row--align-left\">\n        <div class=\"follower\">\n            <span class=\"follower__icon follower__icon--near ion-plus-circled\"></span>\n            <span class=\"follower__text\">15</span>\n            <span class=\"follower__label follower__label--darken\">Followers</span>\n        </div>\n    </div>\n    <div class=\"header__row\">\n        <ul class=\"tab tab--with-background tab--with-shadow\">\n            <li class=\"tab__item tab__item--selected\" data-target=\"tab-mobile-about\">about</li>\n            <li class=\"tab__item\" data-target=\"tab-mobile-setting\">settings</li>\n            <li class=\"tab__item\">option 1</li>\n            <li class=\"tab__item\">option 2</li>\n            <li class=\"tab__item\">option 3</li>\n        </ul>\n    </div>\n</div>\n<div class=\"content card\">\n    <div id=\"tab-mobile-about\" class=\"tab__content tab__content--selected\">\n        <div class=\"content__row content__row--title\">\n            <span class=\"content__title\">About</span>\n            <span class=\"content__view edit-label__button edit-label__button--small ion-edit\" onclick=\"window.util.switchProfileEdit()\"></span>\n            <span class=\"content__edit content--hide content__button\" onclick=\"window.util.switchProfileView()\">Cancel</span>\n            <span class=\"content__edit content--hide content__button content__button--end\" onclick=\"window.util.updateProfile(); window.util.switchProfileView()\">Save</span>\n        </div>\n        <div class=\"content__row\">\n            <span class=\"content__view name content__text content__text--highlight\">{firstName} {lastName}</span>\n        </div>\n        <div class=\"content__row content__row--no-margin content__edit content--hide\">\n            <div class=\"edit-text edit-text--full-width edit-text--near content__edit content--hide\">\n                <input id=\"txt-first-name\" class=\"fname edit-text__text edit-text__text--full-width\" type=\"text\" value=\"{firstName}\" required/>\n                <label class=\"edit-text__label\">First Name</label>\n            </div>\n        </div>\n        <div class=\"content__row content__row--no-margin content__edit content--hide\">\n            <div class=\"edit-text edit-text--full-width edit-text--near content__edit content--hide\">\n                <input id=\"txt-last-name\" class=\"lname edit-text__text edit-text__text--full-width\" type=\"text\" value=\"{lastName}\" required/>\n                <label class=\"edit-text__label\">Last Name</label>\n            </div>\n        </div>\n        <div class=\"content__row\">\n            <span class=\"content__view content__icon ion-android-globe\"></span>\n            <div class=\"edit-text edit-text--full-width edit-text--near\">\n                <input id=\"txt-website\" class=\"edit-text__text edit-text__text--full-width\" disabled type=\"text\" value=\"{website}\" required/>\n                <label class=\"content__edit content--hide edit-text__label\">Website</label>\n            </div>\n        </div>\n        <div class=\"content__row\">\n            <span class=\"content__view content__icon ion-ios-telephone-outline\"></span>\n            <div class=\"edit-text edit-text--full-width edit-text--near\">\n                <input id=\"txt-phone\" class=\"edit-text__text edit-text__text--full-width\" disabled type=\"text\" value=\"{phone}\" required/>\n                <label class=\"content__edit content--hide edit-text__label\">Phone Number</label>\n            </div>\n        </div>\n        <div class=\"content__row\">\n            <span class=\"content__view content__icon ion-ios-location-outline\"></span>\n            <div class=\"edit-text edit-text--full-width edit-text--near\">\n                <input id=\"txt-address\" class=\"edit-text__text edit-text__text--full-width\" disabled type=\"text\" value=\"{address}\" required/>\n                <label class=\"content__edit content--hide edit-text__label\">City, State & Zip</label>\n            </div>\n        </div>\n    </div>\n    <div id=\"tab-mobile-setting\" class=\"tab__content\">\n        <div class=\"content__row content__row--title\">\n            <span class=\"content__title\">Settings</span>\n        </div>\n    </div>\n</div>";

/***/ }),
/* 8 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.initResize = initResize;

var _util = __webpack_require__(1);

var util = _interopRequireWildcard(_util);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function initResize() {
    util.updateUI();
    setResizeListener();
}

function setResizeListener() {
    window.addEventListener("resize", function () {
        util.updateUI();
    });
}

/***/ })
/******/ ]);