import * as tab from "./modules/tab"
import * as popup from "./modules/popup"
import * as util from "./modules/util"
import * as resize from "./modules/resize"
import * as variable from "./modules/variable"

window.tab = tab
window.popup = popup
window.util = util
window.variable = variable

resize.initResize()

