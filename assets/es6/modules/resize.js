import * as util from "./util"

export function initResize(){
    util.updateUI()
    setResizeListener()
}

function setResizeListener(){
    window.addEventListener("resize", function(){
        util.updateUI()
    })
}