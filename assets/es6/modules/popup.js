var popup = document.querySelector(".popup")

const popupTitle = "popup__title"
const popupInput = "popup__input"
const popupClassOpen = "popup--open"
const popupOffsetTop = -10
const popupOffsetLeft = 20

let popupFunction

export function openPopup(target, title = "Title", func){
    document.querySelector(`.${popupTitle}`).innerHTML = title

    closePopup()
    let rect = target.getBoundingClientRect()
    popup.style.top = (rect.top + popupOffsetTop) + "px"
    popup.style.left = (rect.left + popupOffsetLeft) + "px"
    popup.classList.add(popupClassOpen)

    popupFunction = func
}

export function callPopupFunc(){
    popupFunction()
    closePopup()
}


export function closePopup(){
    popup.classList.remove(popupClassOpen)
    resetPopupInput()
}

export function getPopupInput(){
    return document.querySelector(`.${popupInput}`).value
}

function resetPopupInput(){
    document.querySelector(`.${popupInput}`).value = ''
}