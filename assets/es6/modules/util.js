import * as tab from "./tab"
import * as template from "./template"
import * as variable from "./variable"

let rootHTML = document.querySelector(".root")
const rootDesktopClass = "desktop"
const rootMobileClass = "mobile"

const contentView = "content__view"
const contentEdit = "content__edit"
const contentHide = "content--hide"

/*export function updateUI(value, cls, alt = false) {
    if (value == '') {
        if (alt) alert("Input can't be empty")
        return
    }

    if (cls == "name") {
        let fullname = value.split(" ")
        let fname = fullname[0]
        let lname = fullname.slice(1, fullname.length).join(" ")
        updateUI(fname, "fname")
        updateUI(lname, "lname")
    }

    let els = document.querySelectorAll(`.${cls}`)
    for (let i = 0; i < els.length; i++) {
        if (els[i].tagName == 'INPUT') els[i].value = value
        else els[i].innerHTML = value
    }
}*/

export function updateUI(){
    if(isDesktop()) setDesktopLayout()
    else setMobileLayout()
    tab.initTab()
}

export function updateProfile() {
    let fName = document.querySelector('#txt-first-name').value
    let lName = document.querySelector('#txt-last-name').value
    let website = document.querySelector('#txt-website').value
    let phone = document.querySelector('#txt-phone').value
    let address = document.querySelector('#txt-address').value

    if (fName == '' && lName == '' && website == '' && phone == '' && address == '') {
        alert("Input needed for updating profile")
        return
    }

    if (fName != '') variable.setFirstName(fName)
    if (lName != '') variable.setLastName(lName)
    if (website != '') variable.setWebsite(website)
    if (phone != '') variable.setPhone(phone)
    if (address != '') variable.setAddress(address)

    updateUI()
}

function isDesktop(){
    if(screen.width > 767) return true
    return false
}

function isMobile(){
    if(screen.width <= 767) return true
    return false
}

function setDesktopLayout(){
    let tpl = processTpl(template.getDesktopTpl())
    rootHTML.classList.remove(rootMobileClass)
    rootHTML.classList.add(rootDesktopClass)
    rootHTML.innerHTML = tpl
}

function setMobileLayout(){
    let tpl = processTpl(template.getMobileTpl())
    rootHTML.classList.remove(rootDesktopClass)
    rootHTML.classList.add(rootMobileClass)
    rootHTML.innerHTML = tpl
    console.log(tpl)
}

function processTpl(tpl){
    let profileImage = variable.getProfileImage()
    let firstName = variable.getFirstName()
    let lastName =  variable.getLastName()
    let website = variable.getWebsite()
    let phone = variable.getPhone()
    let address = variable.getAddress()

    tpl = tpl.replace(/{profileImage}/gi, profileImage)
    tpl = tpl.replace(/{firstName}/gi, firstName)
    tpl = tpl.replace(/{lastName}/gi,lastName)
    tpl = tpl.replace(/{website}/gi, website)
    tpl = tpl.replace(/{phone}/gi, phone)
    tpl = tpl.replace(/{address}/gi, address)

    return tpl;
}

export function switchProfileEdit() {
    let contentViews = document.querySelectorAll(`.${contentView}`)
    let contentEdits = document.querySelectorAll(`.${contentEdit}`)
    let editTextInputs = document.querySelectorAll('.edit-text__text')

    for (let i = 0; i < contentViews.length; i++) {
        contentViews[i].classList.add(contentHide)
    }

    for (let i = 0; i < contentEdits.length; i++) {
        contentEdits[i].classList.remove(contentHide)
    }

    for (let i = 0; i < editTextInputs.length; i++) {
        editTextInputs[i].disabled = false
    }
}

export function switchProfileView() {
    let contentViews = document.querySelectorAll(`.${contentView}`)
    let contentEdits = document.querySelectorAll(`.${contentEdit}`)
    let editTextInputs = document.querySelectorAll('.edit-text__text')

    for (let i = 0; i < contentViews.length; i++) {
        contentViews[i].classList.remove(contentHide)
    }

    for (let i = 0; i < contentEdits.length; i++) {
        contentEdits[i].classList.add(contentHide)
    }

    for (let i = 0; i < editTextInputs.length; i++) {
        editTextInputs[i].disabled = true
    }
}