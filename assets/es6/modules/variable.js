var profileImage = "/assets/image/profile_image.jpg"
var firstName = "Jessica"
var lastName = "Parker"
var website = "www.seller.com"
var phone = "(949) 325 - 68594"
var address = "Newport Beach, CA"

export function setProfileImage(value){
    profileImage = value
}

export function getProfileImage(){
    return profileImage
}

export function setFullName(value){
    let fullname = value.split(" ")
    firstName = fullname[0]
    lastName = fullname.slice(1, fullname.length).join(" ")
}

export function setFirstName(value){
    firstName = value
}

export function getFirstName(){
    return firstName
}

export function setLastName(value){
    lastName = value
}

export function getLastName(){
    return lastName
}

export function setWebsite(value){
    website = value
}

export function getWebsite(){
    return website
}

export function setPhone(value){
    phone = value
}

export function getPhone(){
    return phone
}

export function setAddress(value){
    address = value
}

export function getAddress(){
    return address
}