let tabs;
const tabClassContent = "tab__content";
const tabSelectedClassItem = "tab__item--selected";
const tabSelectedClassContent = "tab__content--selected";

export function initTab() {
    tabs = document.querySelectorAll(".tab__item");
    for (let i=0; i<tabs.length; i++){
        registerTabListener(tabs[i], "click")
    }
}

function registerTabListener(tab, event) {
    tab.addEventListener(event, () => {
        let dataTarget = tab.getAttribute("data-target")
        let target = document.querySelector(`#${dataTarget}`)

        closeTabAll()
        setTabUnselectedAll()

        showTabTarget(target)
        setTabSelected(tab)
    })
}

function showTabTarget(target){
    if(target != null && target != undefined) target.classList.add(tabSelectedClassContent)
}

function closeTabTarget(target){
    if(target != null && target != undefined) target.classList.remove(tabSelectedClassContent)
}

function closeTabTargetAll(){
    for (let i=0; i<tabs.length; i++){
        let dataTarget = tabs[i].getAttribute("data-target")
        let target = document.querySelector(`#${dataTarget}`)
        closeTabTarget(target)
    }
}

function closeTabAll(){
    let tabContents = document.querySelectorAll(`.${tabClassContent}`)
    for (let i=0; i<tabContents.length; i++){
        closeTabTarget(tabContents[i])
    }
}

function setTabSelected(tab){
    tab.classList.add(tabSelectedClassItem)
}

function setTabUnselected(tab){
    tab.classList.remove(tabSelectedClassItem)
}

function setTabUnselectedAll(){
    for (let i=0; i<tabs.length; i++){
        setTabUnselected(tabs[i])
    }
}

export function changeTab(dataTarget){
    let target = document.querySelector(`#${dataTarget}`)
    closeTabAll()
    showTabTarget(target)
}

