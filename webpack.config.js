var path = require('path');
module.exports = {
    entry: `${__dirname}/assets/es6/app.js`,
    output: {
        path: `${__dirname}/assets/js`,
        filename: 'app.js'
    },
    module: {
        loaders: [
            { 
                test: /\.html$/,
                loader: "html-loader"
            },
            { 
                test: /\.js$/,
                loader: 'babel-loader',
                query: {
                    presets: ['es2015']
                } 
            }
        ]
    }
};